<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDecksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('decks', function (Blueprint $table) {

            $table->id();
            $table->string('name', 100);
            $table->foreignId('user_id')->references('id')->on('users');
            $table->timestamps();
            
        });

        // Schema::table('decks', function (Blueprint $table) {
        //     $table->foreign('user_id')
        //         ->references('id')
        //         ->on('users');
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('decks');
    }
}
