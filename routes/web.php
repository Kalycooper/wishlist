<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'HomeController@index')->name('home');
Route::get('/logout', function() {
    return view('layouts.app');
});

Route::get('auth/google/callback', 'Auth\LoginController@handleProviderCallback');

Route::get('auth/google', 'Auth\LoginController@redirectToProvider')->name('provider');

// Route::any('{query}', function () {
//         return redirect('/');
//     }
// )->where('query', '.*');